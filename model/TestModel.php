<?php

class TestModel extends Model {

	/** 
	 * Проверяет есть ли мейл в базе
	 * @param string $email
	 */

	public function checkEmail($email) 
	{
		$query = "SELECT email FROM Users WHERE email = :email";

		$queryResult = $this->_db->prepare($query);
		$queryResult->execute(array(':email' => $email));

		return count($queryResult->fetchAll()) === 0;
	}

	/**
	 * Симулирует отправку формы
	 */
	public function executeTestApp($email, $text)
	{
		sleep(1);
		return true;
	}
}