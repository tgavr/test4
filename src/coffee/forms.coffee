App.Modules.Forms = class

	###*
		@constructor
		@param {jQuery-Object|HTMLElement} $form Объект формы
		@param {Array} validators валидаторы
	###
	constructor: ($form) ->
		if $form.constructor is $
			@_$form = $form
		else
			@_$form = $ $form

		@_validators = [] 
		@_fieldsCache = {}

	###* Валидирует форму ###
	validate: =>
		(return no unless @_validateField field) for field in @_validators
		return yes

	###*
		Добавляет валидатор
		@param {String} селектор элемента
		@param {String} название валидатора
		@return {App.Modules.Form}
	###
	addValidator: (selector, validator) =>
		@_createField selector
			.validators
			.push validator

		return @

	###* Получение данных с формы ###
	extract: ->
		data = {}
		(data[$(field).attr 'name'] = $(field).val()) for field in @_$form.find 'input,textarea'
		data

	#######################
	#### P R I V A T E ####
	#######################

	_createField: (selector) =>
		(return field if field.selector is selector) for field in @_validators

		field = 
			selector: selector
			validators: []

		@_validators.push field

		field

	###*
		Получение инпута из кэша или из DOM
		@param {String} selector
		@return {jQuery-Object}
	###
	_getField: (selector) =>
		if @_fieldsCache[selector] is undefined
			@_fieldsCache[selector] = @_$form.find selector

		@_fieldsCache[selector]

	###*
		Валидация одного поля
		@param {Object} field
		@return {Boolean}
	###
	_validateField: (field) =>
		$field = @_getField field.selector
		value = @_trim @_extractValue $field

		if not @_checkValue value, field.validators
			#@highlightBadField $field
			return no 
		else
			return yes

	###*
		Обычный trim
		@param {String} строка
		@return {String}
	###
	_trim: (str) ->
		result = str.replace /^\s*/, ""
		i = result.length - 1

		while /\s/.test result[i]
			result = result.splice 0, i
			i--

		result

	###*
		Извлечение валью из поля
		@param {Object} field
		@return {String}
	###
	_extractValue: ($field) ->
		throw Error "No field with selector" unless $field
		$field.val()

	###*
		Проверка одного поля
		@param {String} value Значение поля
		@param {Array} validators Набор валидаторов
		@return {Boolean}

	###
	_checkValue: (value, validators) =>
		(return no unless App.Modules.Forms.validators[validator] value) for validator in validators
		return yes

	#######################
	####  S T A T I C  ####
	#######################
	
	###* Валидаторы ###
	@validators:
		required: (value) -> value.length > 0

		email: (value) -> /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}/.test value

		notags: (value) -> not /<[^>]+>/.test value

	###* Формы ###
	@_$forms: {}

	###*
		Добавление формы
	###
	@addForm: ($form, name, fieldValidates) ->
		name = name || $form.attr('name') || $form.attr('id')
		validates = validates || {}

		throw Error 'Form must be named' unless name

		App.Modules.Forms._$forms[name] = new App.Modules.Forms $form, validates
	
	###*
		Получение формы
	###
	@getForm: (name) ->
		App.Modules.Forms._$forms[name]

	###*
		Удаление формы
	###
	@removeForm: (name) ->
		App.Modules.Forms._$forms[name] = undefined
