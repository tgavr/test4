form = App.Modules.Forms	
	.addForm $(document.forms.test_form)
	.addValidator 'input[name=email]', 'required'
	.addValidator 'input[name=email]', 'email'
	.addValidator 'textarea[name=body]', 'required'
	.addValidator 'textarea[name=body]', 'notags'


onSuccess = ->
	App.Modules.Alert.successAlert '<b>Отлично!</b> Сообщение отправлено'

onError = ->
	App.Modules.Alert.errorAlert '<b>Ошибка!</b> Извините, произошла ошибка сервера'

on409  = ->
	App.Modules.Alert.warningAlert '<b>Предупреждение!</b> Извините, сообщение для такого e-mail уже было отправлено'

on400 = ->
	App.Modules.Alert.errorAlert '<b>Ошибка!</b> Извините, вы заполнили не все поля или ввели недопустимые данные'

checkSettings = 
	url: '/check'
	method: 'get'
	dataType: 'json'
	statusCode:
		400: on400

sendSettings =
	url: '/send'
	method: 'post'
	success: onSuccess
	error: onError
	dataType: 'json'
	statusCode: 
		409: on409
		400: on400

$('#ok').on 'click', () ->
	if form.validate()
		data = form.extract();

		checkSettings.data = email: data.email
		sendSettings.data = data

		$.ajax checkSettings
			.done( (response) -> $.ajax sendSettings if response.result )
			.fail onError
			
	else
		on400()