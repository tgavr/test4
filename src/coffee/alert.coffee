App.Modules.Alert = new class

	constructor: ->

		@_alert = $ "#app_alert"

	successAlert: (text) =>

		@_showAlert('success', text)

	warningAlert: (text) =>

		@_showAlert('warning', text)

	errorAlert: (text) =>

		@_showAlert('danger', text)

	_showAlert: (cssClass, text) =>

		hide = => @_alert.fadeOut 700

		defferedHide = ->
			setTimeout hide, 500

		@_alert
			.html text
			.attr 'class', "alert alert-#{cssClass}"
			.fadeIn 700, defferedHide

		this
