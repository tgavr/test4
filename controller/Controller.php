<?php

class Controller {

	/**
	 * Отрисовывает главную страницу
	 */
	public static function indexAction()
	{
		echo file_get_contents("index.html");
	}

	/**
     * Проверяет мейл на существование в бд
     */
	public static function checkAction()
	{
		$model = new TestModel();
		$email = filter_input(INPUT_GET, "email", FILTER_VALIDATE_EMAIL);

		if ($email === null) {
			header("Status: 400 Bad request");
			return;
		} else {
			echo json_encode(array(
				'result' => $model->checkEmail($_GET["email"])
			));
		}	
	}

	/**
     * Отправлят данные из формы
     */
	public static function sendAction()
	{
		$model = new TestModel();
		$email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
		$text = filter_input(INPUT_POST, "body", FILTER_SANITIZE_STRING);

		$text = trim($text);

		if ($email === null or $text === null or strlen($text) === 0) {
			header("Status: 400 Bad request");
			return;
		} else {
			echo json_encode(array(
				'result' => $model->executeTestApp($_GET["email"])
			));
		}
	}
}