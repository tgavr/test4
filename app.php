<?php
include "engine/config.inc";
include "engine/routing.php";
include "engine/PDOSingletoneWrapper.php";

spl_autoload_register(function($class) {
	if (preg_match("/Model$/", $class)) {
		include "model/" . $class . ".php";
	}
});


include "controller/Controller.php";

$uri = parse_url($_SERVER["REQUEST_URI"]);

$action = $routing[$uri['path']] . "Action";

Controller::$action();