<?php

class PDOSingletoneWrapper {

	/**
	 * Инстанс синглтона
	 * @var PDOSingletoneWrapper
	 */
	private static $_instance;

	/**
	 * Инстанс PDO
	 * @var PDO
	 */
	private $_db;
	
	/** Конструктор */
	private function __construct()
	{
		$dsn = sprintf("%s:dbname=%s;host=%s", DB_DRIVER, DB_NAME, DB_HOST);
		$this->db = new PDO($dsn, DB_USER, DB_PASSWORD);
	}

	/** 
	 * Получить инстанс PDOSingletoneWrapper 
	 * @return PDOSingletoneWrapper
	 */
	public static function getInstance()
	{
		if (self::$_instance === null) {
			self::$_instance = new PDOSingletoneWrapper();
		}

		return self::$_instance;
	}

	/**
	 * Доступ к объекту PDO
	 * @return PDO
	 */
	public function getDB()
	{
		return $this->_db;
	}

}